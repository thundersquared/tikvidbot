const config = {
  mysql: {
    host: 'localhost',
    database: 'tikvidbot',
    user: 'root',
    password: 'root'
  },
  telegram: {
    username: 'tikvidbot',
    token: 'your:token'
  }
}

module.exports = config
